py-coreutils-m
=============
py-coreutils-m is a proof of concept to demo how effective a
multithreaded coreutils could be. Its designed simply to let you
test regular bin utils with its respective multithreaded python
counterpart provided by this package.

rm-m
----
Python multithreaded rm. Use this to test against coreutils rm.

gen-m
-----
Python multithreaded file generation helper. This is no equivalent
for this in coreutils, this is simply provided to help you test
rm-m.

Purpose
-------
The only intended prupose of this tree is to let you benchmark a
multithreaded python implementation of a respective coreutils

ARG_MAX limits
--------------
Before moving on it should be noted rm will complain if the argument list is
too large. Let's say I have 844987 files on test/ directory. Trying to remove
each file with a glob fails with coreutils rm:

	$ rm -f test/*.tmp
	bash: /usr/bin/rm: Argument list too long

In my case this is because of this limit:
	$ getconf ARG_MAX
	2097152

ARG_MAX Is the maximum length of the command line argument. In my case this
is set to 2097152 bytes which is 2 MiB. This will then depend on the name
of the files.

When the above limit is reached the amount of files present is still the same:
	$ ls -1 test | wc -l
	844987

Using rm -rf works on the directory though. So will using find:

	find test -mindepth 1 -maxdepth 1 -type f -delete


Testing against SK hynix SC300
------------------------------

We we test against the SK hynix SC300 SSD. First generate a lot of files.
Below is an example with a reasonable number which does not require much
patience.  If you have patience test with 4294967295 instead, which is
UINT_MAX.

First lets try with empty files:

	$ time ./gen-m -c 844987 /mnt/test
	Dir: /mnt/test
	Count: 844987
	Threads: 8
	Files per thread: 105623
	Thread 0 [0:105622]
	Thread 1 [105623:211245]
	Thread 2 [211246:316868]
	Thread 3 [316869:422491]
	Thread 4 [422492:528114]
	Thread 5 [528115:633737]
	Thread 6 [633738:739360]
	Thread 7 [739361:844987]

	real    0m7.592s
	user    0m1.829s
	sys     0m19.664s

	$ time rm -rf /mnt/test/

	real    0m5.127s
	user    0m0.424s
	sys     0m4.548s

Now repeat but with rm-m on the removal:

	$ time ./gen-m -c 844987 /mnt/test
	...
	real    0m7.409s
	user    0m1.927s
	sys     0m20.086s

	$ time ./rm-m /mnt/test/
	Dir: /mnt/test/
	Glob: None
	Files: 844987
	Threads: 8
	Files per thread: 105623
	Thread 0 [0:105622]
	Thread 1 [105623:211245]
	Thread 2 [211246:316868]
	Thread 3 [316869:422491]
	Thread 4 [422492:528114]
	Thread 5 [528115:633737]
	Thread 6 [633738:739360]
	Thread 7 [739361:844986]

	real    0m6.547s
	user    0m2.418s
	sys     0m13.396s

So regular rm is slightly faster on empty files.

What if we increase the file size?

Let's try with 17 bytes.

	$ time ./gen-m -s 17 -c 844987 /mnt/test
	Dir: /mnt/test
	Count: 844987
	Threads: 8
	Files per thread: 105623
	Thread 0 [0:105622]
	Thread 1 [105623:211245]
	Thread 2 [211246:316868]
	Thread 3 [316869:422491]
	Thread 4 [422492:528114]
	Thread 5 [528115:633737]
	Thread 6 [633738:739360]
	Thread 7 [739361:844987]

	real    0m9.496s
	user    0m13.564s
	sys     0m41.721s

This needs about 3.3 GiB of space:

	$ du -hs /mnt/test/
	3.3G    /mnt/test/

Now remove with regular rm.

	$ time rm -rf /mnt/test/

	real    0m7.601s
	user    0m0.462s
	sys     0m6.597s

Now lets try with rm-m. First we generate again:

	$ time ./gen-m -s 17 -c 844987 /mnt/test
	Dir: /mnt/test
	Count: 844987
	Threads: 8
	Files per thread: 105623
	Thread 0 [0:105622]
	Thread 1 [105623:211245]
	Thread 2 [211246:316868]
	Thread 3 [316869:422491]
	Thread 4 [422492:528114]
	Thread 5 [528115:633737]
	Thread 6 [633738:739360]
	Thread 7 [739361:844987]

	real    0m9.499s
	user    0m14.429s
	sys     0m40.932s

Now we remove with rm-m:

	$ time ./rm-m /mnt/test/
	Dir: /mnt/test/
	Glob: None
	Files: 844987
	Threads: 8
	Files per thread: 105623
	Thread 0 [0:105622]
	Thread 1 [105623:211245]
	Thread 2 [211246:316868]
	Thread 3 [316869:422491]
	Thread 4 [422492:528114]
	Thread 5 [528115:633737]
	Thread 6 [633738:739360]
	Thread 7 [739361:844986]

	real    0m7.083s
	user    0m2.688s
	sys     0m22.375s

What if we increase the file size even more. Let's try with 8192 bytes of random
data added to the files.

	$ time ./gen-m -s 8192 -c 844987 /mnt/test
	Dir: /mnt/test
	Count: 844987
	Threads: 8
	Files per thread: 105623
	Thread 0 [0:105622]
	Thread 1 [105623:211245]
	Thread 2 [211246:316868]
	Thread 3 [316869:422491]
	Thread 4 [422492:528114]
	Thread 5 [528115:633737]
	Thread 6 [633738:739360]
	Thread 7 [739361:844987]

	real    0m12.213s
	user    0m10.394s
	sys     0m44.338s

	$ time rm -rf /mnt/test/
	real    0m11.205s
	user    0m0.619s
	sys     0m7.764s

Now lets try again both but with rm-m.

	$ time ./gen-m -s 8192 -c 844987 /mnt/test
	...

	$ time ./rm-m /mnt/test/
	Dir: /mnt/test/
	Glob: None
	Files: 844987
	Threads: 8
	Files per thread: 105623
	Thread 0 [0:105622]
	Thread 1 [105623:211245]
	Thread 2 [211246:316868]
	Thread 3 [316869:422491]
	Thread 4 [422492:528114]
	Thread 5 [528115:633737]
	Thread 6 [633738:739360]
	Thread 7 [739361:844986]

	real    0m7.345s
	user    0m2.633s
	sys     0m24.293s

Turns out rm-m is faster.

Below is a table of further tests with different filesystems and different
sizes. It revevals that btrfs does not work as efficiently with multithreaded
systems. It also implicates a multithreaded rm by default should perhaps not
work with multithreads. How do we resolve this long term?

	SK hynix SC300
	number-of-files filesystem  file-size     gen-m      rm        rm-m
	4294967295      ext4        0             XXX        XXX       XXX
	3461066752      ext4        0             ?          ?         ?
	1730533376      ext4        0             ?          ?         ?
	865266688       ext4        0             ?          ?         ?
	432633344       ext4        0             ?          ?         ?
	216316672       ext4        0             ?          ?         ?
	108158336       ext4        0             XXX        XXX       XXX
	54079168        ext4        0             ?          ?         ?
	27039584        ext4        0             ?          ?         ?
	13519792        ext4        0             4m1.739s   5m43.322s 6m9.130s
	6759896         ext4        0             ?          ?         ?
	3379948         ext4        0             0m39.885s  0m59.349s 1m7.526s
	3379948         xfs         0             2m52.310s  0m40.210s 1m5.454s
	3379948         ext4        93397         18m..XXX   XXX       XXX
	1689974         ext4        0             0m16.486s  0m23.431s 0m24.226s
	844987          ext4        0             ?          0m5.127s  0m6.547s
	844987          ext4        4096          ?          0m7.601s  0m7.083s
	844987          ext4        17            ?          0m7.601s  0m7.083s
	844987          xfs         17            0m23.750s  0m22.283s 0m21.308s
	844987          btrfs       17            0m17.642s  0m14.870s 0m20.964s
	844987          ext4        8192          ?          0m11.205s 0m7.345s
	844987          xfs         8192          0m26.206s  0m20.480s 0m19.969s
	844987          btrfs       8192          0m33.182s  0m20.320s 0m26.514s

XXX means I gave up given it was taken for ever, or I ran out of disk space.

Testing against Samsung SSD 850
-------------------------------
On a other SSDs or non-SSDs you get worst performance with a multithreaded rm
all around.

	$ time ./gen-m -c 844987 test
	Dir: test
	Count: 844987
	Threads: 8
	Files per thread: 105623
	Thread 0 [0:105622]
	Thread 1 [105623:211245]
	Thread 2 [211246:316868]
	Thread 3 [316869:422491]
	Thread 4 [422492:528114]
	Thread 5 [528115:633737]
	Thread 6 [633738:739360]
	Thread 7 [739361:844987]

	real    0m20.101s
	user    0m14.501s
	sys     0m54.565s

This verifies the mount of files we requested matches what is in the
directory:

	$ ls -1 test | wc -l
	844987

Using rm -rf let us time the removal:

	$ time rm -rf test/

	real    0m14.690s
	user    0m0.570s
	sys     0m6.296s

Now time to test the python multithreaded rm.

	$ time ./rm-m test
	Dir: test
	Glob: None
	Files: 844987
	Threads: 8
	Files per thread: 105623
	Thread 0 [0:105622]
	Thread 1 [105623:211245]
	Thread 2 [211246:316868]
	Thread 3 [316869:422491]
	Thread 4 [422492:528114]
	Thread 5 [528115:633737]
	Thread 6 [633738:739360]
	Thread 7 [739361:844986]

	real    0m25.369s
	user    0m2.697s
	sys     0m23.552s

Now lets try adding some random content to the files. We start off with
8192 byte sized files, and 844987 files. You'll need at least 6.5 GiB of
free space.

	$ time ./gen-m -s 8192 -c 844987 test
	Dir: test
	Count: 844987
	Threads: 8
	Files per thread: 105623
	Thread 0 [0:105622]
	Thread 1 [105623:211245]
	Thread 2 [211246:316868]
	Thread 3 [316869:422491]
	Thread 4 [422492:528114]
	Thread 5 [528115:633737]
	Thread 6 [633738:739360]
	Thread 7 [739361:844987]

	real    0m14.455s
	user    0m11.043s
	sys     0m47.191s

	$ du -hs test
	6.5G    test

Now lets remove this with regular rm -rf.

	$ time rm -rf test

	real    0m19.242s
	user    0m0.550s
	sys     0m10.151s

Repeating the generation again so we can then test with rm-m.

	$ time ./gen-m -s 8192 -c 844987 test
	real    0m13.760s
	user    0m11.047s
	sys     0m45.934s

Now with rm-m:

	$ time ./rm-m test
	Dir: test
	Glob: None
	Files: 844987
	Threads: 8
	Files per thread: 105623
	Thread 0 [0:105622]
	Thread 1 [105623:211245]
	Thread 2 [211246:316868]
	Thread 3 [316869:422491]
	Thread 4 [422492:528114]
	Thread 5 [528115:633737]
	Thread 6 [633738:739360]
	Thread 7 [739361:844986]

	real    0m29.811s
	user    0m3.246s
	sys     0m44.789s

Regular rm is still faster.
